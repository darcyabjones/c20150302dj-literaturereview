# Literature review

- 3000 words
- Worth 10% of final mark
- Due: week 4
- Submit 2 hardcopies to Tony Gendall and a electronic version to Kim

In a literature review, the writer should be able to show that they have studied the work in the field they are reviewing with "insight".
This means that you don't just show what others in the field have discovered, internationally, but also synthesise and critically evaluate the material.

Having a read of you review, a reader should have a sound understanding of:

- What is already known about the topic:
  - What studies have been done in the past on the topic?
  - What are the general principles those sudies have revealed?
  - What key questions or issues have past studies attempted to address on this topic?
- Where possible, you should identify key hypotheses that have been postulated as answers to the key questions identified above.
- Your review should also identify and highlight current gaps in knowledge or understanding of the topic or issue.
- What intellectual frameworks or approaches have generally applied when thinking or studying the topic or issue in the past?
  Have most of the studies been descriptive or have manipulative experiments or comparative studies been conducted?
- What field methods have been used in the past to study this point?
- What are the strengths and limitations of the methods that have been used in the past?

Your review should finish with one or two paragraphs highlighting:

- What you believe are the critical or interesting questions (and possible hypotheses) that remain to be addressed on the topic.
- What you believe would be fruitful methods of tackling those questions.
